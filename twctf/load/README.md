Load: pwnable, warmup
----------------------------------------------------------------------------
Analysis
----------------------------------------------------------------------------

This binary takes in a 128 length filename and uses to open() and read() function to read contents from it. The length and offset can be controlled freely by the user, which causes a buffer overflow. To control the overflowed buffer content we read from /dev/stdin. (/dev/stdin is a symlink to /proc/self/fd/0) We can enter 0 for the offset and an sufficient length so the ROP chain will fit in. However, right before the binary terminates the binary closes stdin, stdout and sterr using the close() function. Therefore information disclosure and further input is impossible.


First Option
----------------------------------------------------------------------------

We considered opening /dev/stdin again, but this did not work because obviously /dev/stdin is a symlink to /self/proc/fd/0 which is already closed. In the local environment it was possible to open /dev/pts/0, /dev/pts/1, /dev/pts/2 which correspond to stdin, stdout, stderr respectively. However in the remote server it did not work. Therefore instead of obtaining a shell or using an open-read-write chain, we thought of other options.


Usable Gadgets
----------------------------------------------------------------------------

By using the magic gadget in libc_csu_init, we can gain rdi, rsi and rdx control. (Rdx control is a bit more complicated yet you can see how it is done in my solve.py)
Functions that can be used are open, read, strchr, start, atoi ..etc. Functions that print to the screen like puts() or printf() are useless since stdout is closed. Strchr returns a NULL when the char argument is not found within the string. Otherwise it returns a pointer within the string that matches the character. There is a gadget that dereferences rax, and when strchr fails to find a character this gadget will cause a segmentation fault for it will attempt to dereference the address 0.
We create the ROP chain that determines if flag[idx] == C.

****************************************************************************
open("flag.txt",0) = 0
****************************************************************************
repeat read(0,buf,1) for (idx+1) times. -> in buf, flag[idx] will be stored.
****************************************************************************
strchr(buf,C) 
****************************************************************************
rax dereferencing gadget
****************************************************************************
some ROP chain that does something...
****************************************************************************

If flag[idx] == C, some ROP chain that does something will be exectued. Otherwise it will cause a segmentation fault and abort immediately. We can put a ROP chain that takes a lot of time to execute (an infinite loop using atoi and jmp rax gadget; set rax to the address of jmp rax gadget using atoi and jump to the jmp rax gadget)

By using these chains I could obtain the flag letter by letter.

TWCTF{procf5_15_h1ghly_fl3x1bl3}

